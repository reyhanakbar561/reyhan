# Ujian Praktik Semester 1

1. Fork project Git yang ada di link pada akun git
   kamu, ubahlah nama project dengan nama singkatmu
   https://gitlab.com/irfanrona/latihan01.git

2. Clone project git yang ada di akun kamu
   `git clone https://gitlab.com/{usernameKamu}/namaKamu.git`

3. Ikuti tahapan ke #6 Tutorial pada link berikut
   https://www.petanikode.com/codeigniter-model/

4. ubahlah nama database pada tutorial di atas
   menjadi `namaKamu` (mengerjakan ini dapat +15 poin)

5. Lakukan commit dengan komentar 'tutorial #6'

6. git push setelah tahapan tutorial berhasil

7. Lakukan git pull (pada project kalian di hari pertama)

8. Ikuti tahapan ke #6 Tutorial pada link berikut
   https://www.petanikode.com/codeigniter-admin/

9. Lakukan commit dengan komentar 'tutorial #7'

10. Lakukan git push setelah tahapan tutorial berhasil

## Bapa akan menilai melalui project git kalian
