<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Siswa extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  $this->load->model('siswa_model');
	}

	public function index()
	{
		
		$data['query'] = $this->siswa_model->read();
		$this->load->view('templates/auth_header');
		$this->load->view('siswa/index',$data);
		$this->load->view('templates/auth_footer');	
	}
	 
	public function edit($nisn)
	{
		$data['query'] = $this->siswa_model->read_by_id($nisn);	
		 $this->load->view('templates/auth_header');
		 $this->load->view('siswa/edit',$data);
        $this->load->view('templates/auth_footer');		
	}

	public function update()
	{
		$nisn = $this->input->post('nisn');
		$nis = $this->input->post('nis');
		$nama = $this->input->post('nama');
		$id_kelas = $this->input->post('id_kelas');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$id_spp = $this->input->post('id_spp');

		echo $nisn . " - " . $nis . " - " . $tahun . " - " . $nama . " - " . $id_kelas . " - " . $alamat . " - " . $no_telp  . " - " . 	$id_spp;

		$data['query'] = $this->siswa_model->update();

		redirect('siswa');
	}

	public function  add()
	{
		$this->load->view('siswa/add');
	}

	public function insert()
	{
		$nisn = $this->input->post('nisn');
		$nis = $this->input->post('nis');
		$nama = $this->input->post('nama');
		$id_kelas = $this->input->post('id_kelas');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		$id_spp = $this->input->post('id_spp');

		echo $nisn . " - " . $nis . " - " . $nama . " - " . $id_kelas . " - " . $alamat . " - " . $no_telp  . " - " . 	$id_spp;

		$data['query'] = $this->siswa_model->create();

		redirect('siswa');
	}

	public function delete($nisn)
	{
		$this->siswa_model->delete($nisn);
		redirect('siswa');
	}
}
