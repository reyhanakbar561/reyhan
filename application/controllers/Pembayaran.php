<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Pembayaran extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  $this->load->model('pembayaran_model');
	}

	public function index()
	{
		
		$data['query'] = $this->pembayaran_model->read();
		$this->load->view('pembayaran/index',$data);
	}

	public function edit($id_pembayaran)
	{
		$data['query'] = $this->pembayaran_model->read_by_id($id_pembayaran);
		$this->load->view('pembayaran/edit',$data);	
	}

	public function update()
	{
		$id_pembayaran = $this->input->post('id_pembayaran');
		$id_petugas = $this->input->post('id_petugas');
		$tgl_bayar= $this->input->post('tgl_bayar');
		$bulan_dibayar= $this->input->post('bulan_dibayar');
		$tahun_dibayar= $this->input->post('tahun_dibayar');
		$id_spp= $this->input->post('id_spp');
		$jumlah_bayar= $this->input->post('jumlah_bayar');

		echo $id_pembayaran. " - " . $id_petugas . " - " . $tgl_bayar . " - " . $bulan_dibayar . " - " . $tahun_dibayar . " - " . $id_spp . " - " . $jumlah_bayar;

		$data['query'] = $this->pembayaran_model->create();

		redirect('pembayaran');
	}

	public function  add()
	{
		$this->load->view('pembayaran/add');
	}

	public function insert()
	{
		$id_pembayaran = $this->input->post('id_pembayaran');
		$id_petugas = $this->input->post('id_petugas');
		$tgl_bayar= $this->input->post('tgl_bayar');
		$bulan_dibayar= $this->input->post('bulan_dibayar');
		$tahun_dibayar= $this->input->post('tahun_dibayar');
		$id_spp= $this->input->post('id_spp');
		$jumlah_bayar= $this->input->post('jumlah_bayar');

		echo $id_pembayaran. " - " . $id_petugas . " - " . $tgl_bayar . " - " . $bulan_dibayar . " - " . $tahun_dibayar . " - " . $id_spp . " - " . $jumlah_bayar;

		$data['query'] = $this->pembayaran_model->create();

		redirect('pembayaran');
	}
	
	public function delete($id_pembayaran)
	{
		$this->pembayaran_model->delete($id_pembayaran);
		redirect('pembayaran');
	}
	
}

