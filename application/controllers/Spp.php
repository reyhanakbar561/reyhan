<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Spp extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  $this->load->model('spp_model');
	}

	public function index()
	{
		
		$data['query'] = $this->spp_model->read();
		$this->load->view('templates/auth_header');
		$this->load->view('spp/index',$data);
		$this->load->view('templates/auth_footer');	
	}
	 
	public function edit($id_spp)
	{
		$data['query'] = $this->spp_model->read_by_id($id_spp);
		$this->load->view('templates/auth_header');
		$this->load->view('spp/edit',$data);	
		$this->load->view('templates/auth_footer');	
	}

	public function update()
	{
		$id_spp = $this->input->post('id_spp');
		$tahun = $this->input->post('tahun');
		$nominal = $this->input->post('nominal');

		echo $tahun . " - " . $nominal;

		$data['query'] = $this->spp_model->update();

		redirect('spp');
	}

	public function  add()
	{

		$this->load->view('spp/add');
	}

	public function insert()
	{
		$tahun = $this->input->post('tahun');
		$nominal = $this->input->post('nominal');

		echo $tahun . " - " . $nominal;

		$data['query'] = $this->spp_model->create();

		redirect('spp');
	}

	public function delete($id_spp)
	{
		$this->spp_model->delete($id_spp);
		redirect('spp');
	}
}

