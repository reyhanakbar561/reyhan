<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
    table,
    th,
    td{
        border: solid black
    }
</style>

<body> 
<div class="container">
    <div class="row justify-content-center">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                 <div>
                 <h1>Data Siswa</h1>

    <table class="table table-dark table-hover">
    <h6> <a href="siswa/add">TAMBAH DATA SISWA</a> </h6>
        <thead>
        <tr>
            <td>nisn</td>
            <td>nis</td>
            <td>nama</td>
            <td>id_kelas</td>
            <td>alamat</td>
            <td>no_telp</td>
            <td>id_spp</td>
            <td>aksi</td>

        </tr>
        </thead>
        <tbody>
        <?php foreach ($query->result() as $row)  { ?>
            <tr>
                <td><?= $row->nisn?></td>
                <td><?= $row->nis ?></td>
                <td><?= $row->nama?></td>
                <td><?= $row->id_kelas?></td>
                <td><?= $row->alamat?></td>
                <td><?= $row->no_telp?></td>
                <td><?= $row->id_spp?></td>
                <td>
                    
                <a href="<?= base_url('siswa/edit/') .  $row->nisn ?>">Edit</a>
                <a href="<?= base_url('siswa/Delete/') .  $row->nisn ?>"><div class="btn btn-danger btn-sm"><i class="fa fa-trash>">Delete</a></div></i>

                </td>
            </tr>
            <?php } ?>
        </tbody>
    <table>    
                 </div>
             </div>
         </div>
     </div>
 </div>

   
</body>

</html>